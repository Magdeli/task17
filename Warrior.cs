﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Warrior : Character
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019

        // This class is a subclass of Character.

        #region Attributes
        #endregion


        #region Constructors

        public Warrior(string name, int energy, int healthPoints, int armorRating) : base(name, energy, healthPoints, armorRating)
        { }

        #endregion


        #region Behaviours
        #endregion
    }
}
