﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class WhiteWizard : Wizard
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019

        // This class is a subclass of Wizard

        #region Attributes
        #endregion


        #region Constructors

        public WhiteWizard(string name, int energy, int healthPoints, int armorRating) : base(name, energy,healthPoints, armorRating)
        { }

        #endregion


        #region Behaviours
        #endregion
    }
}
