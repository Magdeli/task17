﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public abstract class Wizard : Character
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019

        // This class is a subclass of Character. It is abstract, meaning it is only possible to
        // create objects of this class through it's subclasses

        #region Attributes
        #endregion


        #region Constructors

        public Wizard(string name, int energy, int healthPoints, int armorRating) : base(name, energy, healthPoints, armorRating) { }

        #endregion


        #region Behaviours
        #endregion
    }
}
