﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Thief : Character
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019

        // This class is a subclass of Character.

        #region Attributes
        // Here the name is stated only because it is used in the overrided method in this class

        private string name;

        #endregion


        #region Constructors

        public Thief(string name, int energy, int healthPoints, int armorRating) : base(name, energy, healthPoints, armorRating)
        {
            this.name = name;
        }

        #endregion


        #region Behaviours
        // The method Move() from Character is overrided here

        public override void Move()
        {
            Console.WriteLine($"{name} sneaked.");
        }

        #endregion
    }
}
