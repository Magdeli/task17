﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Character
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019


        #region Attributes
        // Each character has the properties name, energy, healthpoints and armorrating

        private string name;
        private int energy;
        private int healthPoints;
        private int armorRating;

        #endregion
        

        #region Constructors
        // This class only have one constructor, and it takes in a name, an energyvalue, a healthpointvalue
        // and an armorratingvalue

        public Character(string name, int energy, int healthPoints, int armorRating)
        {
            this.Name = name;
            this.Energy = energy;
            this.HealthPoints = healthPoints;
            this.ArmorRating = armorRating;
        }

        #endregion


        #region Behaviours

        #region Get and set
        // These can be used to get or set any of the variables of a Character object

        public int Energy { get => energy; set => energy = value; }
        public string Name { get => name; set => name = value; }
        public int HealthPoints { get => healthPoints; set => healthPoints = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }

        #endregion

        // This method allows the character to move
        public virtual void Move()
        {
            Console.WriteLine($"{name} moved.");
        }

        // This method allows the character to attack
        public void Attack()
        {
            Console.WriteLine($"{name} attacked!");
        }

        // This method creates a string with a summary of the character's attributes
        public string PrintSummary()
        {
            string summary = ($"Name: {name}\nEnergy: {energy}\nHP: {healthPoints}\nArmorrating: {armorRating}");
            return summary;
        }

        #endregion
    }
}
