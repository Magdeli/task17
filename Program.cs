﻿using System;

namespace Task17
{
    public class Program
    {
        static void Main(string[] args)
        {
            Wizard gandalf = new WhiteWizard("Gandalf", 100, 100, 20);

            gandalf.Move();

            Warrior nick = new Warrior("Nick", 150, 100, 80);

            Console.WriteLine(nick.Name);

            Thief loke = new Thief("Loke", 100, 120, 40);

            loke.Attack();
            loke.Move();

            Wizard oldGandalf = new GreyWizard("Old Gandalf", 50, 50, 20);
            Console.WriteLine(oldGandalf.ArmorRating);

            Console.WriteLine(gandalf.PrintSummary());

        }
    }
}
